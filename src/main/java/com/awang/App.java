package com.awang;

/**
 * Hello world!
 *
 */
import java.time.*;
import java.time.temporal.Temporal;
import java.util.*;

import static java.time.Duration.between;
import static sun.java2d.cmm.ColorTransform.In;

public class App 
{
    HashMap<Integer,ArrayList<Record>> employeeRecords = new HashMap<>();
    static Employee addEmployee(Scanner scanner) throws InvalidIdInputException{
        System.out.println("=== Create New Account ==" + "\n==========================");
        System.out.println("Type in your Employee ID: ");

        String input = scanner.nextLine();
        Integer employeeID;
        if(canConvertToInt(input)){
            employeeID = Integer.parseInt(input);
        } else {
            throw new InvalidIdInputException();
        }

        System.out.println("Type in your first name: ");
        String firstName = scanner.nextLine();
        System.out.println("Type in your last name: ");
        String lastName = scanner.nextLine();

        //Create new Employee
        Employee employee = new Employee();
        employee.setId(employeeID);
        employee.setFirstName(firstName);
        employee.setLastName(lastName);
        employee.setDateCreated(LocalDate.now());
        return employee;
    }

    static Integer login(Scanner scanner, ArrayList<Employee> employeeList) throws InvalidIdInputException{
        //Boolean validID = false;
        System.out.println("\nPlease enter your employee ID: ");

        Integer employeeID = Integer.parseInt(scanner.nextLine());
        for(Integer i = 0; i< employeeList.size(); i++){
            Integer empListId = employeeList.get(i).getId();
            if(empListId.equals(employeeID)){
                System.out.println("Logging in...");
                return employeeID;
            }
        }
        System.out.println("Login failed: Invalid ID");
        throw new InvalidIdInputException();
    }

    static void manageRecord(Scanner scanner, HashMap<Integer,ArrayList<Record>> records, Integer employeeID){
        //lets user create, delete, or view a record
        Boolean logout = false;

        while(logout == false){
            System.out.println("=== Timesheet Menu ===" + "\nType the number of your selection:\n [1] Create new entry {2] View timesheet [3] Delete entry [4] Logout");
            String choice = scanner.nextLine();
            if(choice.equals("1")){
                Record newRecord = new Record();
                System.out.println("=== Create new entry ===\nType in date: in this format MM/DD/YYYY  hh:mm");
                //Type in date
                String[] dateAndTime = scanner.nextLine().split(" ");
                String[] dateValues = dateAndTime[0].split("/");

                LocalDate recordDate = LocalDate.of(Integer.parseInt(dateValues[2]), Month.of(Integer.parseInt(dateValues[1])),Integer.parseInt(dateValues[0]));

                //Type in start time
                System.out.println(" Type in your start time (HH:MM : ");
                String startTimeInput = scanner.nextLine();
                //String[] startTimeString = startTimeInput.split(":");
                LocalTime startTime = LocalTime.parse(startTimeInput);

                //Type in end time
                System.out.println(" Type in your end time HH:MM : ");
                String endTimeInput = scanner.nextLine();
                //String[] endTimeString = startTimeInput.split(":");
                LocalTime endTime = LocalTime.parse(endTimeInput);

                //Calculate elapsed time
                long hoursWorked = Duration.between(startTime, endTime).toHours();

                //Set new record object
                newRecord.setDate(recordDate);
                newRecord.setEmployeeId(employeeID);
                newRecord.setTimeIn(startTime);
                newRecord.setTimeOut(endTime);
                newRecord.setHoursWorked(hoursWorked);

                //Add to records
                records.get(employeeID).add(newRecord);
            }
            else if(choice.equals("2")){
                System.out.println("=== View timesheet ===");
                ArrayList<Record> employeeRecords = records.get(employeeID);
                for(int i = 0; i < records.get(employeeID).size(); i++){
                    System.out.println("[" + (i+1) + "] Time In:" + employeeRecords.get(i).getTimeIn() + "    Time Out: " + employeeRecords.get(i).getTimeOut());
                }
            }
            else if(choice.equals("3")){
                System.out.println("=== Delete entry ===");
                //Type in numeric selection which day you want to delete
                String entryNumberString = scanner.nextLine();
                Integer entryNumber = Integer.parseInt(entryNumberString);
                Record selectedEntry = records.get(employeeID).get(entryNumber-1);
                System.out.println("Removing your entry... Date[" + selectedEntry.getDate() + "] Time In[" + selectedEntry.getTimeIn() + "] Time Out[" + selectedEntry.getTimeOut() + "]");
                records.get(employeeID).remove(entryNumber-1);
            }
            else if(choice.equals("4")){
                System.out.println("Logout");
                logout = true;
            }
            else {
                System.out.println("Invalid. Select one of the options [1-4]");
            }
        }
        return;
    }

    static Boolean canConvertToInt(String s){
        if(s == null){
            return false;
        }
        try{
            Integer.parseInt(s);
        } catch(NumberFormatException e){
            return false;
        }
        return true;
    }

    public static void main( String[] args )
    {
        try(Scanner scanner = new Scanner(System.in)) {
            Integer loggedInId = 0;
            Boolean isLoggedInTimeSheetManagement = false; 
            Boolean exitTimeSheet = false;
            ArrayList<Employee> employees = new ArrayList<Employee>();
            HashMap<Integer,ArrayList<Record>> employeeRecords = new HashMap<>();
            System.out.println("\n\n========================================" +
                    "\n=== Welcome to the Timesheet Program ===" +
                    "\n========================================");
            while(!exitTimeSheet == true){
                try{
                    System.out.println("\nType the number of your selection:\n [1] Login [2] Create New User [3] Exit");
                    String input = scanner.nextLine();
                    if(input.equals("1")){
                        Integer loginedId = login(scanner, employees);
                        manageRecord(scanner, employeeRecords, loginedId);
                    }
                    else if(input.equals("2")){
                        Employee employee = addEmployee(scanner);
                        employees.add(employee);
                        employeeRecords.put(employee.getId(),new ArrayList<Record>());
                    }
                    else if(input.equals("3")){
                        System.out.println("Bye");
                        exitTimeSheet = true;
                    }
                    else {
                        System.out.println("****************************************\nInvalid. Select one of the options [1-3]\n****************************************");
                    }

                } catch(InvalidIdInputException inputException){
                    System.out.println("Invalid Employee ID. Must be an integer");
                }

            }
        } catch(Exception e){
            System.out.println("Invalid input " + e);
        }
    }
}
